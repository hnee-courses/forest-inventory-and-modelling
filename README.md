## Readme for Forest Inventory and Modelling

Here is the assignment for the *Forest Inventory and Modelling* course. It used data obtained during measurements under the Polish National Forest Inventory. The data include the parameters of trees with a diameter at the breast height thicker than 7 cm over bark and the parameters of the sample plots. 

our partial assignments are as follows:

* summary presentation of available data in tabular or graphical form
* Calculation of the volume of single trees with the use of:
  * The generalized height-diameter curves
  * Formulae for the merchantable wood form factor
  * Biomass of tree components – 
    * allometric equations: leaves, (different) tree stump, roots, (branches)
  * Biomass in roots
  * Carbon content in tree components
  
In all cases please write a short discussion/summary as well as a short summary for the entire project. 

The minimum requirement for passing the course is to perform all analyses and add a minimum of explanation/discussion to it.

The maximum grade is reserved for those, who perform analyses, **provide deep analysis and interpretation of available information** as well as reveal a deep understanding of the solved problems (including problems, assumptions, limitations and justification of methods).

The complete project is due by 5 September (but preferably by the end of June) and submit into "Assignment_files" available on Moodle Platform in the general section. Available file formats: PDF, DOC(X) or ODT format.



## setup of R
install the packages using the setup.R script


## setup of python
install the packages from requirements.txt

Run the script "tree_form_factor.py"