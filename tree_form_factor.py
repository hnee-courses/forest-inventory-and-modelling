"""
calculate the metrics for forest inventory
"""

import math

import pandas as pd
from loguru import logger
import matplotlib.pyplot as plt
import seaborn as sns

with open("data/trees_group_1.csv", "r") as f:
    df = pd.read_csv(f)
    print(df)

## only use cycle 1
df = df[df["NR_CYKLU"] == 1]



## TODO do some EDA
df_aggregate = df[["GAT", "PID"]].groupby("GAT").count()

## calculate the average height of the stand
h_avg = df[df["H"] > 0]["H"].mean()
dict_avg_height = df[df["H"] > 0][["GAT", "H", "d13_cm"]].groupby(["GAT"]).mean().to_dict() ## get the average height per group
dict_n_trees = df[df["H"] > 0][["GAT", "H"]].groupby(["GAT"]).count().to_dict() # trees which have a height given

n_trees = len(df[df["H"] > 0]["H"])
logger.info(f"average height the stand: {h_avg} of {n_trees} trees   ")

## get the average centimeter
D = df[df["d13_cm"] > 0]["d13_cm"].mean()
logger.info(f"average diameter {D} TODO: the formula in the document is wrong")
logger.warning(
    f"TODO: is it a good idea to calculate with different sets? height is not availabe for many trees"
)


def form_factor_f1(x):
    """
    calculate stem form factor
    :param x:
    :return:
    """
    d = x["d13_cm"]

    species = x["GAT"]
    if species == "SO":
        f_1 = 1 / (1 + (d / (1.2895 + 0.90645 * d)) ** 4)

    elif species == "SW":
        f_1 = 0.34 + 0.648 / math.sqrt(d)

    elif species == "JD":
        f_1 = 0.4132 + 0.4779 / math.sqrt(d) + 0.4426 * h_avg**-1.6259 * (D - d)

    elif species in ["DB", "JS"]:
        f_1 = 0.5441 * d ** (-0.0415)

    elif species in ["BK"]:
        f_1 = 0.46 * D ** (-0.008) + (0.0059 - 0.0001 * D) * (D - d)

    else:
        f_1 = 0

    return f_1


def form_factor_s(x):
    """
    calculate ratio of merchantable wood volume and stem volume
    :param x:
    :return:
    """
    d = x["d13_cm"]  ## FIXME, maybe we need the centimeters??
    species = x["GAT"]
    if species == "SO":
        s = ((d - 6) / (0.2834 + 0.988 * (d - 6))) ** 4

    elif species == "SW":
        s = 1 - 225.73 * (d - 1) ** -3.2542

    elif species == "JD":
        s = 1 - 559.4519 * d** -3.5946

    elif species in ["DB", "JS"]:
        s = ((d - 3) / (0.9549 + 0.9439 * (d - 3))) ** 4

    elif species in ["BK"]:
        s = 1.1168 - 48.115 / d**2


    else:
        s = 0
    return s


def form_factor(x):
    """
    calculate the actual form factor

    :param x:
    :return:
    """
    ## Form % 20 factor.pdf
    f_q = x["s"] * x["f1"]
    return f_q

def generalized_height_curve(x):
    """
    calculate the height for any tree using the Näslund function:
    using volume modeling/Procedures for calculation.pdf
    :param x:
    :return:
    """
    species = x["GAT"]
    d = x["d13_cm"]

    if species in ["SO", "LB", "SOW", "SOC", "MD"]:
        # Scots pine, Swiss pine, eastern white pine, black pine, larch
        O = 0.7445
        r = -0.4531


    elif species in ["SW"]:
        # Norway Spruce
        O = 0.3341
        r = -0.2344

    elif species in ["JD"]:
        # White fir
        O = 0.2566
        r = -0.1420

    elif species in ["DG"]:
        # douglas fir
        O = 0.2822
        r = -0.1979

    elif species in ["DB", "DBC"]:
        # Oak, red oak
        O = 0.7895
        r = -0.4696

    elif species in ["BK", "JW", "JS", "OL", "KL"]:
        # Beech, sycamore, ash, alder, maple
        O = 0.6582
        r = -0.4197

    elif species in ["GB", "WZ", "JRZ", "WB", "CZR"]:
        # Hornbeam, elm, mountain ash, willow, bird cherry
        O = 0.8992
        r = -0.5300


    elif species in ["LP"]:
        # lime
        O = 0.2140
        r = -0.0905

    else:
        h = 0
        a = 0
        b = 0
        O = 0
        r = 0

    h_avg_group = dict_avg_height["H"][species]
    D_avg_group = dict_avg_height["d13_cm"][species]

    if species in ["BRZ"]:
        b = 0.364043 - 0.375941 * math.sqrt(h_avg_group)
    else:
        b = O * h_avg_group ** r
    a = D_avg_group / math.sqrt(h_avg_group - 1.3) - b * D_avg_group
    h = (d / (a + b * d))**2 + 1.3

    return h


def volume_function(x):
    """
    calculate the volume
    :param x:
    :return:
    """

    d_cm = x["d13_cm"]
    h_est = x["h_est"]
    fq = x["fq"]
    ## two steps in one pi*r^2 == pi*d^2/4
    ## to get cubic meter from centimeter 100^2 = 10000
    ## 4 * 10000 == 40000
    volume = (math.pi / 40000) * d_cm**2 * h_est * fq

    return volume

def biomass_aboveground_AB(x):
    """
    calculate the stem biomass
    :param x:
    :return:
    """
    species = x["GAT"]
    d_cm = x["d13_cm"]

    if species in ["SO", "LB", "SOW", "SOC", "MD"]:
        ## 344 from zianis, actually only for scots pine, but who has time for that...
        # Scots pine, Swiss pine, eastern white pine, black pine, larch
        a = 200.87186
        b = 124.68
        c = 49
        AB = a * d_cm**2 + b*(d_cm**2-c)

    elif species in ["SW"]:
        ## Norway Spruce
        ## zianis 148 for AB
        a = -60.55702
        b = 5.46558
        c = 0.27567
        AB = a + b * d_cm + c * d_cm**2

    elif species in ["BK"]:
        # beech 88
        a = 0.453
        b = 2.139

        AB = a * d_cm ** b

    elif species in ["JD"]:
        # White fir
        O = 0.2566
        r = -0.1420
        AB = -1

    elif species in ["DG"]:
        # douglas fir
        O = 0.2822
        r = -0.1979
        AB = -1 # TODO implement me

    elif species in ["GB", "WZ", "JRZ", "WB", "CZR", "DB", "DBC"]:
        # Hornbeam, elm, mountain ash,
        # willow, bird cherry, Oak, red oak
        # zianis 556
        a = 0.2306
        b = 2.2729
        AB = a * d_cm ** b
    elif species in ["LP"]:
        # lime
        O = 0.2140
        r = -0.0905
        AB = -1 # TODO implement me

    else:
        AB = -1 # TODO implement me

    return AB

def biomass_Coarce_roots(x):
    """
    calculate the biomass for roots
    :param x:
    :return:
    """
    """
    In this example the biomass of coarse roots (RC) is calculated by using the function of row 442 in the appendix of *Zianis et al. (2005)*:
    """
    species = x["GAT"]
    d_m = x["d13_cm"]/100
    d_cm = x["d13_cm"]
    h_est = x["h_est"]

    if species in ["SW"]:
        a = 7.33
        b = 1.383
        ## formula 244 from zianis
        root_biomass = a * (h_est*d_m**2)**b

    elif species in ["BK"]:
        ## 115
        ## logarithm of beech
        a = -4.1302
        b = 2.6099
        root_biomass = math.exp(a + b * math.log(d_cm))
    else:
        # TODO distinguish by species
        a = 3.129
        b = -0.536
        c = 0.077
        d = -0.036642
        d_cm = x["d13_cm"]
        h_est = x["h_est"]

        root_biomass = a + b * d_cm + c * d_cm ** 2 + d * d_cm * h_est

    return root_biomass



df["s"] = df.apply(form_factor_s, axis=1)
df["f1"] = df.apply(form_factor_f1, axis=1)
df["fq"] = df.apply(form_factor, axis=1)
df["h_est"] = df.apply(generalized_height_curve, axis=1)
df["v"] = df.apply(volume_function, axis=1)

## TODO implement more of the biomass equations
df["m_AB"] = df.apply(biomass_aboveground_AB, axis=1)
df["m_RC"] = df.apply(biomass_Coarce_roots, axis=1)

## TODO some statistics, are there trees with no height? Yes, why? in R: summary(df)
df["h_err_perc"] = df.apply(lambda x: x.h_est / x.H  if x.H is not None else None, axis=1)
df_height_eval = df[df["h_err_perc"] > 0][["GAT", "H", "h_est", "h_err_perc"]] # calculate the difference in height estimations
df_height_eval["h_diff_perc"] = abs(df_height_eval["h_err_perc"] - 1)
df_height_eval_mean = df_height_eval.groupby(["GAT"]).mean()

df_mass_eval = df[["GAT", "H", "h_est", "v", "m_AB", "m_RC"]] # calculate the difference in height estimations
df_mass_eval = df_mass_eval.groupby(["GAT"]).mean()


## compare the real height with estimated height
height_plot = sns.scatterplot(data=df_height_eval, x="H", y="h_est")
fig = height_plot.get_figure()
fig.savefig("doc/height_plot.png")
# plt.show()

# Draw a nested barplot by species and sex
fig, ax1 = plt.subplots(figsize=(10, 10))
tidy = df_height_eval[["GAT", "H", "h_est"]].melt(id_vars='GAT').rename(columns=str.title)
tidy = tidy[tidy["Gat"].isin(["SW", "JD", "BK", "JRZ", "JW", "IWA"])]
sns.barplot(x='Gat', y='Value', hue='Variable', data=tidy, ax=ax1)
sns.despine(fig)
fig.savefig("doc/grouped_barchart.png")

plt.show()

## WARNING: do not look at the mean height and estimated height. The diff is correct, because it calculated on the individual rows
logger.info(f"errors in percent for height: {df_height_eval_mean.to_dict()}")
df_height_eval_groupby = df_height_eval.groupby(["GAT"])["GAT"].count().to_dict()
logger.info(f"occurences of 'SW', 'JD', 'BK' are the highest.")
summary = df.describe()
summary = summary.transpose()
print(summary.head())



with open("data/trees_group_1_form_factor_height_volume.csv", "w") as f:
    df.to_csv(f, index=False)
    logger.info(f"wrote height and volume estimation with form factor.")



